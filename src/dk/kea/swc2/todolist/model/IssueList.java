package dk.kea.swc2.todolist.model;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Scanner;

/**
 * This class defines a List of Issue objects
 * @author  Cristiana Man
 * @version 1.0
 */
public class IssueList {
    private static ArrayList<Issue> issueList = new ArrayList<>();
    
    public IssueList() {
        read();
    }

    public static ArrayList<Issue> getIssueList() {
        return issueList;
    }

    public static void setIssueList(ArrayList<Issue> issueList) {
        IssueList.issueList = issueList;
    }
    
    public static Issue get(int index) {
        return issueList.get(index);
    }
    
    public static void add(Issue i) {
        issueList.add(i);   //add the issue into the issueList
    }
    
    public static void remove(int index) {
        issueList.remove(index);
    }
    
    public static int size() {
        return issueList.size();
    }
    
    /**
     * This method reads from input.txt 
     * Each 3 lines represent fields from an instance of Issue
     * Issues are added to issueList
     */
    
    public void read() {
        try {
            Scanner anIssue = new Scanner(new File("input.txt")); //read from input.txt
            while(anIssue.hasNext()) {                              //while we have lines in the file
                String name = anIssue.nextLine();                   //read properties of an issue into some variables
                
                String remarks = "";
                String remarksLine = anIssue.nextLine();            //remarks may have more lines, end sequence "!@#$%^&*" shows when remarks field ends
                while(anIssue.hasNext() && !remarksLine.equals("!@#$%^&*")) {
                    remarks += "\n" + remarksLine;
                    remarksLine = anIssue.nextLine();
                 }
                                
                int status = anIssue.nextInt(); 
                               
                add(new Issue(name,remarks,status));   //add the issue into the issueList
                
                if(anIssue.hasNext())
                    anIssue.nextLine(); //it goes into next line after reading an int
            }
        }
        catch(FileNotFoundException e) {
            System.out.println("in read() File was not found!");
        }
    }
      
    /**
      * Creates a PrintStream object
      * The input.txt will be overwritten with edited ArrayList
      * Each array element is saved on 3 lines representing issues name, reamarks and status
      */

    public static void save() {
      try {
          PrintStream out = new PrintStream(new File("input.txt"));
          int size = issueList.size();
          for(int i = 0; i < size; i++) {
              Issue item = get(i);
              out.println(item.toStringFile());
          }

      } catch(FileNotFoundException e) {
          System.out.println("in save() File was not found!");
      } 

    }
}
