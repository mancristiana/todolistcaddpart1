package dk.kea.swc2.todolist.model;

/**
 * This class defines the Issue object
 * @author  Cristiana Man
 * @version 1.0
 */
public class Issue {
    //fields
    private String name;
    private String remarks;
    private int status; //status can have 3 states: 1=new; 2=in progress; 3=done; 
    

    
    //CONSTRUCTORS
    //default values for an issue
    public Issue() {
        this("Untitled_" + IssueList.size() ,"",1);
    }

    public Issue(String name, String remarks, int status) {
        setName(name);
        setRemarks(remarks);
        setStatus(status);
    }
  
    //SETTERS & GETTERS
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        if(status < 1 || status > 3)
            status = 1;
        this.status = status;
    }

    @Override
    public String toString() {
        return "Issue{" + "name=" + name + ", remarks=" + remarks + ", status=" + status + '}';
    }
    
    public String toStringFile() {
        return name + "\n" + remarks + "\n!@#$%^&*\n" + status;
    }
    
}
